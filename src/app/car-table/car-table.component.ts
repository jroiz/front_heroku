import { Component, OnInit } from '@angular/core';
import { Car } from 'src/entities/car.model';
import { CarService } from '../car.service';

@Component({
  selector: 'app-car-table',
  templateUrl: './car-table.component.html',
  styleUrls: ['./car-table.component.css'],
})
export class CarTableComponent implements OnInit {
  cars: Car[] = [];
  selectedCar: Car = {
    id: -1,
    created: new Date('1970-01-01T00:00:00'),
    updated: new Date('1970-01-01T00:00:00'),
    registration: new Date('1970-01-01T00:00:00'),
  };
  constructor(public carService: CarService) {}

  ngOnInit(): void {
    this.carService.getCars().subscribe((cars) => (this.cars = cars));
  }
  carSelected(car: Car): void {
    this.selectedCar = car;
    this.openPopup();
  }
  displayStyle = 'none';

  openPopup() {
    this.displayStyle = 'block';
  }
  closePopup() {
    this.displayStyle = 'none';
  }
  deleteCar() {
    this.carService.deleteCar(this.selectedCar.id).subscribe((car) => {
      if (car != null) {
        this.cars = this.cars.filter((c) => c !== this.selectedCar);
        this.closePopup();
      }
    });
  }
}
