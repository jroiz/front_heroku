import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of } from 'rxjs';
import { Car } from 'src/entities/car.model';
import { FormObject } from 'src/entities/form-object.model';

const carUrl: string = 'http://localhost:8080/formacion/services/cars';
@Injectable({
  providedIn: 'root',
})
export class CarService {
  constructor(private http: HttpClient) {}

  getCars(): Observable<Car[]> {
    return this.http.get<Car[]>(carUrl).pipe(
      map((cars) => {
        cars.forEach((car) => {
          car = this.fixCarDates(car);
        });
        return cars;
      }),
      catchError(this.handleError<Car[]>('getCars', []))
    );
  }

  deleteCar(id: Number): Observable<Car> {
    return this.http
      .delete<Car>(`${carUrl}/${id}`)
      .pipe(catchError(this.handleError<Car>('deleteCar')));
  }

  getSingleCar(id: Number): Observable<Car> {
    return this.http
      .get<Car>(`${carUrl}/${id}`)
      .pipe(
        map((car) => {
          car = this.fixCarDates(car);
          return car;
        }),
        catchError(this.handleError<Car>('getSingleCar'))
      );
  }
  updateCar(data: FormObject): Observable<Car> {
    if (data.registration != null) {
      data.registration = data.registration + 'T00:00:00';
    }
    console.log(data);
    return this.http.put<Car>(carUrl, data, ).pipe(
      map((car) => {
        car = this.fixCarDates(car);
        return car;
      }),
      catchError(this.handleError<Car>('updateCar'))
    );
  }
  addCar(data: FormObject): Observable<Car> {
    var body = {
      country: data.country,
      brand: data.brand,
      registration: data.registration + 'T00:00:00',
    };
    console.log(body);
    return this.http.post<Car>(carUrl, body).pipe(
      map((car) => {
        car = this.fixCarDates(car);
        return car;
      }),
      catchError(this.handleError<Car>('addCar'))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private fixCarDates(car: Car): Car {
    car.created = new Date(car.created.toString().substring(0, 19));
    car.updated = new Date(car.updated.toString().substring(0, 19));
    car.registration = new Date(car.registration.toString().substring(0, 19));
    return car;
  }
}
