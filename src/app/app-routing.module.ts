import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarFormComponent } from './car-form/car-form.component';
import { CarTableComponent } from './car-table/car-table.component';

const routes: Routes = [
  {
    path: 'add-car',
    component: CarFormComponent,
  },
  {
    path: 'car-table',
    component: CarTableComponent,
  },
  {
    path: '',
    redirectTo: 'car-table',
    pathMatch: 'full',
  },
  {
    path: 'edit-car/:id',
    component: CarFormComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
