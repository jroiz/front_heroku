import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { FormObject } from 'src/entities/form-object.model';
import { CarService } from '../car.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.css'],
})
export class CarFormComponent implements OnInit {
  @Input() car: FormObject = {};
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public carService: CarService,
    private location: Location
  ) {}

  ngOnInit(): void {
    if (this.router.url != '/add-car')
      this.route.params
        .pipe(
          switchMap((params: Params) =>
            this.carService.getSingleCar(+params['id'])
          )
        )
        .subscribe((car) => {
          this.car.id = car.id;
          this.car.country = car.country;
          this.car.brand = car.brand;
          this.car.registration = car.registration
            .toISOString()
            .substring(0, 10);
        });
  }
  addCar() {
    this.carService.addCar(this.car).subscribe((car) => {
      if (car != null) {
        this.location.back();
      }
    });
  }
  updateCar() {
    this.carService.updateCar(this.car).subscribe((car) => {
      if (car != null) {
        this.location.back();
      }
    });
  }
}
