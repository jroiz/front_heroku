export class FormObject {
  id?: Number;
  country?: string;
  brand?: string;
  registration?: string = '';
}
