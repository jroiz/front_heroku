export class Car {
  id: Number = -1;
  country?: string;
  brand?: string;
  created: Date = new Date('1970-01-01T00:00:00');
  updated: Date = new Date('1970-01-01T00:00:00');
  registration: Date = new Date('1970-01-01T00:00:00');
}
